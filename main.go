package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	//You can add the type as name of the file
	contactInfo
}

func main() {
	jim := person{
		firstName: "Jim",
		lastName:  "Party",
		contactInfo: contactInfo{
			email:   "JimParty@fakemail.fake",
			zipCode: 94000,
		},
	}

	jimPointer := &jim
	jimPointer.updateName("Jimmy")
	jim.print()
}

//*Type means that the type is a pointer of that type --> (pointerToPerson *person) means that pointerToPerson is a
//pointer to a type person in the memory
func (pointerToPerson *person) updateName(newFirstName string) {
	//When we have a * before a pointer we're asking to transform that pointer in the value they're pointing to
	(*pointerToPerson).firstName = newFirstName
}

func (p person) print() {
	fmt.Printf("%+v", p)
}
